package com.example.labo2.controller;


import com.example.labo2.Entities.Book;
import com.example.labo2.Services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@CrossOrigin(origins = "http://localhost:3000")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping(value="/newBook",consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void createBook( @Valid @RequestBody Book book) {
        bookService.saveBooks(book);
    }

    @GetMapping(value="/allBooks")
    @ResponseStatus(HttpStatus.OK)
    public  @ResponseBody List<Book> getAllBooks() {
        List<Book> books = (List<Book>) bookService.listAllBooks();
        return books;
    }

    @GetMapping(value="/getById/{id}")
    @ResponseStatus(HttpStatus.OK)
    public  @ResponseBody Book getBookById(@PathVariable Integer id) {
        Book book = bookService.getBook(id);
        return book;
    }

    @GetMapping(value="/allBooksByAuthor/{author}")
    @ResponseStatus(HttpStatus.OK)
    public  @ResponseBody List<Book> getAllBooksByAuthor(@PathVariable String author) {
        List<Book> books = (List<Book>) bookService.getBookByAuthor(author);
        return books;
    }

    @DeleteMapping(value="/deleteBook/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody void deleteBook(@PathVariable Integer id) {
        bookService.deleteBook(id);
    }



    @PutMapping(value = "/editBook/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody void editBook(@PathVariable Integer id, @Valid @RequestBody Book book ) {
//        Book bk = bookService.getBook(id);
//
//        updateAuthor(book, bk);
//        updateDescription(book, bk);
//        updateStatus(book, bk);
//        updateTitle(book, bk);

        bookService.saveBooks(book);
//        return bookService.getBook(id);
    }

    private void updateTitle(@RequestBody @Valid Book book, Book bk) {
        if(book.getTitle() != ""){
            bk.setTitle(book.getTitle());
        }
    }

    private void updateStatus(@RequestBody @Valid Book book, Book bk) {
        if(book.getStatus() != ""){
            bk.setStatus(book.getStatus());
        }
    }

    private void updateDescription(@RequestBody @Valid Book book, Book bk) {
        if(book.getDescription()!= ""){
            bk.setDescription(book.getDescription());
        }
    }

    private void updateAuthor(@RequestBody @Valid Book book, Book bk) {
        if(book.getAuthor() != ""){
            bk.setAuthor(book.getAuthor());
        }
    }
}
