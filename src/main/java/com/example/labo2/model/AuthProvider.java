package com.example.labo2.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
