package com.example.labo2.Repository;


import com.example.labo2.Entities.Genre;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface GenreRepository extends CrudRepository<Genre, Integer>{
}
