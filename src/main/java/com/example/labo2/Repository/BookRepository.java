package com.example.labo2.Repository;


import com.example.labo2.Entities.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface BookRepository extends CrudRepository<Book, Integer> {

    @Query("SELECT b FROM Book b WHERE b.author LIKE %:author%")
    Iterable<Book> findByAuthor(@Param("author") String author);
}
