package com.example.labo2.Services;


import com.example.labo2.Entities.Genre;

public interface GenreService {
    Iterable<Genre> listAllGenre();
}
